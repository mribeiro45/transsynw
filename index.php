<html>
<head>
  <title>TransSynW</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.0.3/purify.min.js"></script>
</head>
<body>
  <header>
    <div class="container" style="width: 890px;">
      <div class="row">
        <div class="col-sm-12">
          <a class="navbar-brand" href="http://www.uni.lu"><img class="img-responsive" src="images/UL_logo_80x80.png" height=75px width=75px></a>
          <a class="navbar-brand" href="https://wwwen.uni.lu/lcsb"><img class="img-responsive" src="images/LCSB_logo.png" height=90px width=90px ></a>
          <a class="navbar-brand" href="https://www.fnr.lu/"><img class="img-responsive" src="images/FNR_logo.png" width =260px></a>
        </div>
      </div>
    </div>
  </header>
  <div class="jumbotron container" style="width: 890px;">
    <div class="row no gutter">
      <style>
        .col-custom {
          width: 24%;
          padding-left: 15px;
        }
      </style>
      <div class="col-custom">
        <p></p>
        <h1>TransSynW</h1>
        <p></p>
        <p class="lead">Single cell-based computational tool for subpopulation conversion</p>
      </div>
      <div class="col">
        <img src="images/image.png" class="img-fluid" alt="Responsive Image" height=375px width=625px>
      </div>
    </div>
    <br>
    <style>
        .col-custom2 {
          width: 50%;
          padding-left: 0px;
        }
    </style>
    <div class="col-custom2">
        <button type="button" class="btn btn-light btn-sm" data-toggle="collapse" data-target="#collapseExample" role="button" aria-expanded="false">More details</button>
        <a href="tutorial.php" class="btn btn-light btn-sm" role="button" target="_blank">Tutorial</a>
    </div>
    <div class="collapse" id="collapseExample">
      <div class="card card-body">
        <font size="2" class="text-justify">
        <h5>About</h5>
        <p>Convert between cell subtypes by using single-cell data to identify an optimal, subtype-specific set of core transcription factors personalized for your own human or mouse cell study.</p>
        <h5>File formats</h5>
          <h6>Input</h6>
          <ul>
          <li><u>Starting cell population:</u> Select or upload a tab-separated file with the single-cell gene expression data from the cell type you started your differentiation protocol from. Genes should be labeled according to the Gene Symbols nomenclature.<br>For optimal results, the platform used to sequence the starting and the target cell types should be the same.</li>
           <li><u>Gene expression matrix:</u> Upload a tab-separated file of your raw (unnormalized) single-cell data where each column represents a cell with an unique name and the rows contain gene expression values. Genes should be labeled according to the Gene Symbols nomenclature.<br>For a faster upload, please consider submitting a gene expression matrix containing only transcription factors or run the code locally <a href="https://git-r3lab.uni.lu/mariana.ribeiro/transsynw" target="_blank">[repository here]</a>.</li>
           <li><u>Cell annotation:</u> Upload a tab-separated file where the first row matches the unique cell names in the gene expression matrix and the second row corresponds to the cluster to which each cell belongs. For analysing two or more clusters as one, check the box “I would like to merge the selected subpopulations”. </li>
          </ul>
          <p><b>Examples</b>: Please select "Preview Examples" button in the main page to find gene expression matrix and cluster information format examples, obtained from <a href="https://www.cell.com/cell/fulltext/S0092-8674(16)31309-5" target="_blank">La Manno <i>et al.</i> 2016</a> data.</p>
          <h6>Output</h6>
          <p>The results are compressed in a .zip file. Inside, you will find a summary file that contains the metadata of your analysis, an hierarchical clustering dendrogram of your gene expression matrix, and two tables: "cores.tsv” containing the predicted cell conversion TFs for each target subpopulation, ranked by the fold-change; and “markers.csv” containing the top 10 predicted marker genes of each target subpopulation, ranked by JSD score (low JSD = unique and specific marker).</p>
        <h5>Authors</h5>
        <p>This software was developed in the <a href="https://wwwen.uni.lu/lcsb/research/computational_biology" target="_blank">Computational Biology Group</a> by <a href="https://wwwen.uni.lu/lcsb/people/mariana_ribeiro" target="_blank">Mariana Ribeiro</a>, <a href="https://wwwen.uni.lu/lcsb/people/satoshi_owaka" target="_blank">Dr. Satoshi Owaka</a> and <a href="https://wwwen.uni.lu/lcsb/people/antonio_del_sol_mesa" target="_blank">Prof. Dr. Antonio del Sol</a>.</p>
        <h5>Citation</h5>
        <p>Within any publication that uses any methods or results derived from or inspired by TransSynW, please cite:<br>
        <a href="https://stemcellsjournals.onlinelibrary.wiley.com/doi/full/10.1002/sctm.20-0227" target="_blank">Ribeiro MM, Okawa S, del Sol A. TransSynW: A single-cell RNA-sequencing based web application to guide cell conversion experiments. STEM CELLS Transl Med. 2020;1–9.</a><p>
      </div>
    </font>
    </div>
    <hr>
    <form method="POST" action="index.php" enctype="multipart/form-data" class="form-horizontal">
      <div class="form-group">
      <select class="custom-select" name=strpop required>
        <option selected>Select starting cell type</option>
        <optgroup label="10x Genomics">
          <option value="hIPSC_E-MTAB-6687_10x.Robj">Human induced pluripotent stem cells (hiPSC)</option>
          <option value="hPBMC_10x_5k_pbmc_v3_filtered.Robj">Human peripheral blood mononuclear cells (PBMC)</option>
          <option value="MEF_GSE103221_10x.Robj">Mouse embryonic fibroblasts (MEF)</option>
          <option value="hLymphocytes_GSM3589419_10x.Robj">Human lymphocytes</option>
        </optgroup>
        <optgroup label="Cell-Seq">
          <option value="mESC_GSE54695_celseq.Robj">Mouse embryonic stem cells (mESC)</option>
        </optgroup>
        <optgroup label="Drop-seq">
          <option value="HDF_GSE126042_dropseq.Robj">Human dermal fibroblasts (HDF)</option>
        </optgroup>
        <optgroup label="Fluidigm C1">
          <option value="HFF_GSE75748_fluidigmC1.Robj">Human foreskin fibroblasts (HFF)</option>
          <option value="MEF_GSE67310_fluidigmC1.Robj">Mouse embryonic fibroblasts (MEF)</option>
          <option value="hESC_GSE75748_H1_fluidigmC1.Robj">Human embryonic stem cells (hESC)</option>
          <option value="mESC_GSE75804_fluidigmC1.Robj">Mouse embryonic stem cells (mESC)</option>
          <option value="mKeranocytes_GSE67602_fluidigmC1.Robj">Mouse keranocytes (mKC)</option>
        </optgroup>
        <optgroup label="Microfluidics">
          <option value="mESC_GSE47835_microfluidic.Robj">Mouse embryonic stem cells (mESC)</option>
        </optgroup>
        <optgroup label="Smart-seq2">
          <option value="hESC_E-MTAB-6819_H9_naive_smartseq2.Robj">Human embryonic stem cells (hESC)</option>
          <option value="mESC_GSE74534_smartseq2.Robj">Mouse embryonic stem cells (mESC)</option>
        <option value="mAstro_GSE114000_smartseq2.Robj">Mouse astrocytes (mAstro)</option>
        </optgroup>
      </select>
      or
      <div class="input-group">
      <div class="custom-file" id=file3>
        <input type="file" class="custom-file-input" id="customFile3" name="customFile3" accept=".txt,.csv,.tsv">
        <label class="custom-file-label" id=customFile-label3 for="customFile3">Upload your starting cell type in tab-delimited format</label>
      </div>
      </div>
      <script type="application/javascript">
        $('input#customFile3').change(function(e){
            var fileName3 = e.target.files[0].name;
            $('label#customFile-label3').html(fileName3);
        });
      </script>
      </div>
      <script>
      jQuery(function ($) {
        var $inputs = $('select[name=strpop],input[id=customFile3]');
        $inputs.on('input', function () {
        // Set the required property of the other input to false if this input is not empty.
        $inputs.not(this).prop('required', !$(this).val().length);
        });
      });
      </script>
      <hr>
      <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
            <select class="custom-select" name=species>
                <option selected value="Human">Human</option>
                <option value="Mouse">Mouse</option>
            </select>
            </div>
        <div class="custom-file" id=file2>
          <input type="file" class="custom-file-input" id="customFile2" name="customFile2" accept=".txt,.csv,.tsv,.Rds" onchange="javascript:updateList()" required>
          <label class="custom-file-label" id=customFile-label2 for="customFile2">Upload <b>gene expression matrix</b> in tab-delimited format</label>
          <script type="application/javascript">
            $('input#customFile2').change(function(e){
              var fileName2 = e.target.files[0].name;
              $('label#customFile-label2').html(fileName2);
          });
          </script>
        </div>
        </div>
      </div>
      <div class="form-group">
      <div class="input-group">
      <div class="custom-file">
        <input type="file" class="custom-file-input" id="customFile" name="customFile" accept=".txt,.csv,.tsv" required>
        <label class="custom-file-label" id=customFile-label for="customFile">Upload <b>cell annotation</b> file in tab-delimited format</label>
      </div>
      <script type="application/javascript">
        $('input#customFile').change(function(e){
            var fileName = e.target.files[0].name;
            $('label#customFile-label').html(fileName);
        });

        function handleFileSelect(evt) {
          var file = evt.target.files[0]; // FileList object
          var reader = new FileReader();

          reader.onload = function(e) {
            var text = reader.result.split('\n');   // split the entire file by lines
            var firstLine = text.shift(); // first line
            var secondLine = text.shift();
            var subpop = secondLine.split('\t');
            uniq = [...new Set(subpop)];
            placeContent(document.getElementById('subpop'), uniq);
          }

          function placeContent(target, content) {
            var output = [];
            if(content.length < 2) {
              window.alert("No clusters found. \n \n Please upload a file with two or more clusters.");
              } else {
                output.push('<h6>Which subpopulations would you like to analyze?</h6>');
                for (var i = 0; i < content.length; i++) {
                  output.push('<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" name=subpopulations[] value="',DOMPurify.sanitize(content[i]),'" id="',i,'"><label class="custom-control-label" for="',i,'">',DOMPurify.sanitize(content[i]),'</label></div>');
                }
                output.push('<br>');
                output.push('<div class="form-check"><input class="form-check-input" type="checkbox" value="yes" name=merge id="defaultCheck1"><label class="form-check-label" for="defaultCheck1">I would like to merge the selected subpopulations</label></div>');
              }
              target.innerHTML = '<div class="p-3">' + output.join('') + '</div>';
            }
            reader.readAsText(file, 'UTF-8');
            }
            document.getElementById('customFile').addEventListener('change', handleFileSelect, false);
      </script>
    </div>
   </div>
   <hr>
   
      <div class="row">
        <div class="col" id="subpop">
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col">
            <input type="email" class="form-control" name="Email" id="Email" aria-describedby="emailHelp" placeholder="Enter your email to receive the results" required>
          </div>
          <div class="col-4">
            <button type="submit" name="Submit" id="mysubmit" class="btn btn-primary btn-block">Submit</button>
            <small id="submitHelp" class="form-text text-muted">By clicking "Submit", you agree to our <a href="termsofuse.php">Terms of Use.</a></small>
            <script type="application/javascript">
                var toValidate = jQuery('#customfile, #Email, #customfile2'),
                    valid = false;
                toValidate.keyup(function () {
                    if (jQuery(this).val().length > 0) {
                        jQuery(this).data('valid', true);
                    } else {
                        jQuery(this).data('valid', false);}
                    toValidate.each(function () {
                        if (jQuery(this).data('valid') == true) {
                            valid = true;
                        } else {
                            valid = false;}
                    });
                    if (valid === true) {
                        jQuery("#mysubmit").prop('disabled', false);
                    } else {
                        jQuery("mysubmit").prop('disabled', true);}
                });
            </script>
          </div>
        </div>
      </div>
    </form>
    <div class="row-4">
      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Preview Examples</button>
        <div class="dropdown-menu btn-block" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" target="_blank" href="images/example_expression-matrix.png">Gene expression matrix</a>
          <a class="dropdown-item" target="_blank" href="images/example_cluster-info.png">Cell annotation file</a>
          <a class="dropdown-item" target="_blank" href="example_data.zip">Download example dataset</a>
        </div>
      </div>
    </div>
    <br>
    <div class="alert alert-secondary" role="alert">Thank you for your submission.<br>Once the page refreshes, this window may be closed.</div>
    </div>
    
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="text-center">
          <small>Copyright © <a href="http://www.uni.lu">Université du Luxembourg (2019)</a>. All rights reserved.<br>
                Designed by <a href="https://wwwen.uni.lu/lcsb/people/mariana_ribeiro">Mariana Ribeiro</a></small>
        </div>
      </div>
    </div>
  </footer>
  
    <?php

    if(isset($_POST["Submit"])) {
      $species = $_POST["species"];
      $filename = $_FILES["customFile2"]["tmp_name"];
      $filename2 = $_FILES["customFile"]["tmp_name"];
      $email = addslashes($_POST["Email"]);

      if(sizeof($_POST['subpopulations']) < 1) {
        echo '<div class="alert alert-danger" role="alert"><h6>No populations were selected, please try again.</h6>';
      } else {
        echo '<div class="alert alert-success" role="alert">Thank you for your submission.<br>You will be notified by email as soon as the results are available.</div>';
        echo '<div class="alert alert-info" role="alert"><h6>You have selected the following subpopulations:</h6>';

        foreach ($_POST['subpopulations'] as $key => $value) {
          echo $value . "<br>";
          $subpopulations = $subpopulations."::".$value;
        }
        $subpopulations = $subpopulations."::".$_POST["merge"];
        echo '</div>';

        # placeholder
        
        if (!empty($_FILES["customFile3"]["tmp_name"])) {
            $filename3 = $_FILES["customFile3"]["tmp_name"];
            $orgname = $_FILES["customFile2"]["name"]. '::' .$_FILES["customFile"]["name"]. '::' .$_FILES["customFile3"]["name"];
            #echo $filename3. " " .$filename. " " .$filename2. " " .$subpopulations. " " .$email. " " .$species. " " .$orgname;
            echo $orgname;
            echo shell_exec('bash -c "exec nohup setsid ./transsyn_wrapper.sh '.$filename3.' '.$filename.' '.$filename2.' '.$subpopulations.' '.$email.' '.$species.' '.$orgname.'"');
        } else {
            $strpop = $_POST["strpop"];
            $orgname = $_FILES["customFile2"]["name"]. '::' .$_FILES["customFile"]["name"];
            #echo $strpop. " " .$filename. " " .$filename2. " " .$subpopulations. " " .$email. " " .$species. " " .$orgname;
            echo shell_exec('bash -c "exec nohup setsid ./transsyn_wrapper.sh '.$strpop.' '.$filename.' '.$filename2.' '.$subpopulations.' '.$email.' '.$species.' '.$orgname.'"');
        }
      }
    }
      ?>

  </div>
</body>
</html>
