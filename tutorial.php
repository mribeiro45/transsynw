<html>
<head>
  <title>TransSynW Tutorial</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.0.3/purify.min.js"></script>
</head>
<body>
  <header>
    <div class="container" style="width: 1080px;">
      <div class="row">
        <div class="col-sm-12">
          <a class="navbar-brand" href="http://www.uni.lu"><img class="img-responsive" src="images/UL_logo_80x80.png" height=75px width=75px></a>
          <a class="navbar-brand" href="https://wwwen.uni.lu/lcsb"><img class="img-responsive" src="images/LCSB_logo.png" height=90px width=90px ></a>
          <a class="navbar-brand" href="https://www.fnr.lu/"><img class="img-responsive" src="images/FNR_logo.png" width =260px></a>
        </div>
      </div>
    </div>
  </header>
  <div class="jumbotron container" style="width: 1080px;">
    <div class="row no gutter">
      <style>
        .col-custom {
          width: 48%;
          padding-left: 15px;
        }
      </style>
      <div class="col-custom">
        <p></p>
        <h1>TransSynW</h1>
        <p class="lead">Single cell-based computational tool for <br> subpopulation conversion</p>
      </div>
      <div class="col-custom">
        <img src="images/image.png" class="img-fluid" alt="Responsive Image" height=375px width=625px>
      </div>
    </div>
    <br>    
    <h5>Tutorial Video</h5>
    <p></p>
    <video height="auto" width="100%" controls><source src="tutorial.mp4"></video>
    <br>
    <br>
    <a href="example_data.zip" target="_blank">Download example data</a>
    <br>
    <p>If you wish to run TransSynW locally, you can find the code repository <a href="https://git-r3lab.uni.lu/mariana.ribeiro/transsynw" target="_blank">here</a>.</p>
    <a href="https://transsynw.lcsb.uni.lu">Return to main page</a>
  </div>
  
  
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="text-center">
          <small>Copyright © <a href="http://www.uni.lu">Université du Luxembourg (2019)</a>. All rights reserved.<br>
                Designed by <a href="https://wwwen.uni.lu/lcsb/people/mariana_ribeiro">Mariana Ribeiro</a></small>
        </div>
      </div>
    </div>
  </footer>
</html>
