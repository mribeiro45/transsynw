# TransSynW

TransSynW is a single cell-based computational tool that identifies subpopulation-specific sets of core transcription factors, optimized for cellular conversion protocols. It was developed and maintained by Computational Biology Group at Luxembourg Centre of Systems Biomedicine.

As key features of this algorithm, TransSynW prioritizes pioneer factors among predicted conversion TFs to facilitate chromatin opening, often required for cell conversion, and predicts marker genes for assessing the performance of cell conversion experiments.

Instructions and tutorials can be found at:
*  https://transsynw.lcsb.uni.lu/


TransSynW is also hosted on GitLab, you can view and clone the repository at:
*  https://git-r3lab.uni.lu/mariana.ribeiro/transsynw

We believe that TransSynW is a valuable tool for guiding researches to design novel protocols for cell conversion in stem cell research and regenerative medicine.
