<html>
<head>
  <title>TransSynW - Terms of Use</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.0.3/purify.min.js"></script>
</head>
<body>
  <div class="jumbotron container">
    <h1>TransSynW</h1><br>
    <div>
    <font class="text-justify">
    <h3>Terms and Conditions:</h3>
    <p> Welcome to TransSynW, a service provided by the Luxembourg Centre for Systems Biomedicine, an interdisciplinary centre of the University of Luxembourg.<br>
      The below Terms and Conditions apply to all agreements by and between the University of Luxembourg and a User (as defined below) and to all legal acts in connection therewith.<br>
      Please read these Terms and Conditions carefully before ordering any TransSynW service. By ordering a TransSynW service, you signify your agreement to these Terms and Conditions. Upon acceptance – by ordering a Service - of these Terms and Conditions, such Terms and Conditions become the Agreement (as defined below) between the University of Luxembourg and the User. If you do not agree to these Terms and Conditions, you may not order any TransSynW service.<br>
      The Service Provider (as defined below) may modify these Terms and Conditions at any time by updating this posting. The User is bound by any such modification and should therefore visit this page periodically to review these Terms and Conditions. The User’s continued use of the TransSynW service after a modification signifies its agreement to the said modification.</p>
      <ol>
        <h5><li>Definition:</h5>
          As used in these Terms and Conditions, the following terms, when used in capital letters, shall have the following meanings:<br>
            <b>Agreement:</b> The agreement by and between the Service Provider and the User, in which the Service Provider commits to perform the services described therein. The modalities of such Agreement are defined by the present Terms and Conditions.<br>
            <b>Applicable Data Protection Laws:</b> Any applicable national law of Luxembourg related to Data Protection and enforcing the General Data Protection Regulation (EU) 2016/679 or any superseding text.<br>
            <b>User:</b> The natural person or legal entity that enters into an Agreement with the Service Provider.<br>
            <b>Service Provider:</b> The Luxembourg Centre for System Biomedicine, an interdisciplinary centre of the University of Luxembourg, an institution of higher education and research, having its registered office at 2 avenue de l’Université, L-4365 Esch-sur-Alzette, Luxembourg.<br>
            <b>Parties:</b> The User and the Service Provider.<br>
            <b>Service:</b> The services performed by the Service Provider in compliance with the Agreement.<br>
            <b>TransSynW:</b> a computational service aiming to identify the synergistic transcriptional cores that determine cell subpopulation identities.<br>
            <b>TransSynW Platform:</b> A website through which the User shall apply for the performance of the Service.<br>
            <b>Results:</b> Results of the Service.<br>
            <b>Set of Data:</b> set of data uploaded on the TransSynW Platform by the User to be processed by the Service Provider. Such Set of Data may contain Personal Data such as, but not limited to, human cell data.<br>
            the following words have the same meaning as in the European Regulation 2016/679 named General Data Protection Regulation (“GDPR”): “Controller”, “Processor”, “Data Subject”, “Personal Data”, “Data Protection Officer”, “Pseudonymisation”, “Consent” and “Processing”.<br>
            <br>
        <h5><li>TransSynW Platform:</h5>
          The User can place an order on the TransSynW Platform. By doing so, the User understands and agrees that the Service Provider might collect and process its Personal Data (as defined by the European Regulation 2016/679 mentioned below) for the purposes performing the Service.<br>
          The User agrees to provide, maintain and update true, accurate, current and complete information about itself when providing the email address where to send the Results. The User agrees not to impersonate any person or entity or misrepresent its identity or affiliation with any person or entity.<br>
          The content available on the TransSynW Platform, such as text, graphics, images, audio, video and other material, as well as the domain names, tagline, organization and user look-and-feel (collectively, the "Content"), is protected by copyright, trademark and other such laws in Luxembourg and foreign countries, and is owned or controlled by the Service Provider or by third parties that have licensed their Content to the Service Provider. Unauthorized use of the Content may violate copyright, trademark, and other laws.<br>
          The TransSynW Platform may contain links to third parties’ websites that are maintained by such third parties. Any such links are provided solely as a convenience to the User and not as an endorsement by the Service Provider of the content on such third parties’ websites. The Service Provider is not responsible for the content of such websites and does not make any representations or warranties regarding the content or accuracy of materials on such third parties’ websites or the privacy practices of such third parties. If the User decides to access to linked third parties’ web sites, the User does so at its own risk.<br>
          <br>
        <h5><li>Placing an order:</h5>
          The User can place an order for the Service by uploading a Set of Data on the TransSynW Platform and providing an email address to which the Results will be sent by the Service Provider upon completion of the Service.<br>
          <br>
        <h5><li>User Obligations:</h5>
          The User shall provide a Set of Data of good quality to the Service Provider. The User understands and acknowledges that the accuracy of the Results depends on the quality of the Set of Data provided.<br>
          The User agrees and acknowledges that any failure to provide, either in quality and/or in quantity, the Set(s) of Data or any other information or material in accordance with the Agreement will delay the Service and that the Service Provider shall not be deemed to be in breach of any of its obligation nor liable to the User in respect of any such failure of the User.<br>
          The User represents and warrants that: (i) it owns or has the right to provide the Set(s) of Data to the Service Provider for use in connection with the Service, and such delivery and use will not violate any obligation that the User owes to any third party; (ii) it has secured all necessary approvals, licenses and permits to deliver the Set(s) of Data to the Service Provider for use in connection with the Service; (iii) the Service Provider’s use of the Set(s) of Data for the performance of the Service will not infringe any intellectual property or other proprietary rights of any third party, nor violate any legal or ethical requirements.<br>
          In the event of any breach of any of the foregoing representations and warranties, the User shall indemnify and hold harmless the Service Provider, and its respective directors, officers, employees, representatives, licensors and agents from and against any loss, costs, damages, liabilities or claims of any kind relating to such breach.<br>
          <br>
        <h5><li>The Service:</h5>
          The Service Provider will use its reasonable skills and due care in performing the Service on behalf of the User. The Agreement with the User shall not be regarded to imply any undertaking to reach a certain result but is at all times an undertaking to use reasonable efforts.<br>
          The Service Provider will use its reasonable skills and due care in performing the Service in a timely manner. The delay in delivering the Results may differ  according to the flow of orders. A specific date of delivery of the Results cannot therefore be agreed upon.<br>
          <br>
        <h5><li>Delivery of Results:</h5>
          When available, the Service Provider will send the Results to the email address the User has provided.<br>
          The Service shall be deemed to be completed upon delivery of the Results. The Service Provider shall have no obligation to perform any further Services on the same Set(s) of Data and shall not be required to deliver any copy of the Results.<br>
          The Service Provider will delete the Set(s) of Data and the email address upon delivery of the Results.<br>
          <br>
        <h5><li>Payment:</h5>
          The Service is provided on a free-of-charge basis.<br>
          <br>
        <h5><li>Ownership and Intellectual Property:</h5>
          The User is responsible for the clearance of the rights on the Set(s) of Data and remains the owner of such Set(s) of Data. The User allows the Service Provider to process the Set(s) of Data for the purpose of the Service.<br>
          The User becomes the owner of the Results upon delivery of such Results.<br>
          The Service Provider shall not violate the intellectual property rights of others. It is the Service Provider's policy, at its discretion and when appropriate, to terminate the account of any User who may infringe, or has infringed the copyrights of third parties.<br>
          <br>
        <h5><li>Use of the Results:</h5>
          The Service aims to provide a tool to support biomedical research. It is NOT approved for clinical use. The User shall use the Results at its own discretion and its own risk.<br>
          The Service Provider makes no representations or warranties about the accuracy, reliability or completeness to be obtained from using the Results.<br>
          <br>
        <h5><li>Data Protection:</h5>
          <u>10.1	Placing an order:</u><br>
          To the extent that the User provides Personal Data to place an order, the Service Provider will be considered as Controller regarding such Personal Data.<br>
          In such case, the Service Provider will collect and store the Personal Data of the User for the sole purpose of delivering the Results to the User. The Personal Data will be stored securely and will be deleted by the Service Provider according to Section 6 of the present Terms and Conditions.<br>
          The processing of your personal data is necessary for the performance of the Terms and conditions in accordance with article 6(1) ( b) of the GDPR.<br>
          According to the GDPR, the User has the right to access to its Personal Data, and to ask the Service Provider for modification or deletion of such Personal Data. In case of any queries regarding his/her personal data the User can contact the Data Protection Officer of the University on <a href="mailto:dpo@uni.lu">dpo@uni.lu</a> during working hours or following the procedure available on <a href="https://wwwen.uni.lu/university/data_protection/your_rights">https://wwwen.uni.lu/university/data_protection/your_rights</a>.<br>
          If you consider the Processing of your Personal Data infringes your rights, you can lodge a complaint to the CNPD. Information is provided on <a href="http://www.cnpd.lu">http://www.cnpd.lu</a>.<br>
          By placing an order, the User expressly acknowledges the collection and processing of its Personal Data.<br>
          <br>
          <u>10.2 Performance of the Service:</u><br>
          For the purpose of the Service, the User is a Data Controller and the Service Provider is a Data Processor.<br>
          By providing the Set of Data, data containing Personal Data might be transferred from the User to the Service Provider. By ordering the Service, the User instructs the Service Provider to Process such Personal Data.<br>
          Each Party shall be responsible for complying with all Data Protection requirements. The User ensures to provide only Personal Data obtained and processed in accordance with the Applicable Data Protection Laws, which have undergone Pseudonymization. The User is responsible for ensuring that the Consent of the Data Subject has been documented and given and that it covers the execution of the Service by the Service Provider, or that the collection and processing of such Data are covered by any other valid legal ground in accordance with the GDPR. The User is responsible for enforcing Data Subject’s statutory rights. If any of these requirements is not met, the Agreement shall be terminated and the Service Provider shall not be held responsible for any loss, harm or damage generated in connection with such termination. The Service Provider is not liable for any misuse of the Personal Data by the User.<br>
          The Service Provider shall only Process Data, which have undergone Pseudonymization and ensures that such Personal Data shall not be stored nor retained during a period of time superior to six (6) months after the date of the delivery of the Results as stated under Section 6. The Service Provider shall ensure that the Processing of the Personal Data is made in compliance with Applicable Data Protection Laws requirements. The Service Provider shall not share any Personal Data and it shall not undertake any action in order to identify any Data Subject.<br>
          Furthermore, the Service Provider as a Processor of the Personal Data warrants and undertakes to:<br>
          a)	Process the Personal Data only upon written and documented instructions, including in electronic form, from the Controller;<br>
          b)	Authorize only employees to Process the Personal Data under the condition that their obligations related to confidentiality are adequate to comply with the GDPR;<br>
          c)	Ensure the confidentiality, the integrity and availability of the Personal Data;<br>
          d)	Assist the Controller in treating Data Subject’s queries, within the limits of the nature to the framework of the Processing;<br>
          e)	Assisting the Controller in ensuring its duties with regards to the statutory rights of the Data Subject;<br>
          f)	Delete all Personal Data contained in the Set(s) of Data after delivery of the Results;<br>
          g)	If required by the Controller, demonstrate the compliance with the above-mentioned measures and allow for audits conducted by the Controller or an auditor mandated by the Controller strictly limited to the matter hereto. The audit shall be undertaken upon at least two weeks of prior notice. The audit will occur during the opening hours of the Processor and will be limited to the Controller’s Personal Data. In no circumstances, the Controller may audit the Personal Data of other Users or access to Personal Data not relevant to the processing of the Services it has asked. The Processor may ask for a fee regarding to the organisation of the audit.<br>
          h)	Inform the Controller if an instruction from it seems to infringe the data protection laws.<br>
          The Controller represents and warrants that information about the presence of Personal Data could be provided with the Set of Data and that the order is either not a further (or “secondary”) processing, or that, if the case, the order complies with articles 5.1, 6.4 and 89 of the GDPR.<br>
          The Parties shall not transfer Personal Data to a country outside of the European Economic Area without complying with applicable data protection laws.<br>
          The Parties agree to cooperate fully with each other for responding to requests from supervisory authorities.<br>
          The Parties shall not release any Data to any person or entity other than those involved in the performance of the Service, and on a strict need-to-know basis.<br>
          <br>
        <h5><li>Liability:</h5>
          The Service Provider shall not be liable for (1) any inaccuracy, error in or failure of the Service, (2) or any loss or damage (including without limitation any consequential, indirect, incidental, special or exemplary damages) arising from such inaccuracy, error in or failure of the Service, (3) or any loss or damage (including without limitation any consequential, indirect, incidental, special or exemplary damages) arising from any download or use made of the Results. If any of the above provisions are void under applicable law, the Service Provider's liability shall be limited to the full extent permitted by law.<br>
          Any liability of the Service Provider, or its employees or affiliated parties, towards the User is at all times limited to direct damages only. It will furthermore at all times be limited to any payment made in that respect by its business liability insurance or professional indemnity insurance as the case may be.<br>
          Any delivery date of the Results agreed upon by the Service Provider is always considered as a guideline or an indication and shall not be considered as binding for the Service Provider.<br>
          <br>
        <h5><li>Indemnity:</h5>
          The User agrees to defend, indemnify, and hold harmless the Service Provider, its officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from the User’s breach of these Terms of Use. This indemnification provision does not apply to intentional or reckless acts or gross negligence on the part of the Service Provider.<br>
          <br>
        <h5><li>Termination:</h5>
          The Service Provider is entitled, without further notice of default and without written intervention, to terminate with immediate effect the Agreement, if the User fails to fulfil any of its obligations under these Terms and Services.<br>
          <br>
        <h5><li>Applicable Laws and Jurisdiction:</h5>
          Any dispute arising from the interpretation and/or implementation of these Terms and Conditions, which cannot be settled amicably, shall be brought before a competent court in the city of Luxembourg.<br>
      </ol>
    </div>
  </div>
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="text-center">
          <small>Copyright © <a href="http://www.uni.lu">Université du Luxembourg (2019)</a>. All rights reserved.<br>
                Designed by <a href="https://wwwen.uni.lu/lcsb/people/mariana_ribeiro">Mariana Ribeiro</a></small>
        </div>
      </div>
    </div>
  </footer>
  </body>
</html>
